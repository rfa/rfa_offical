import React from 'react';
import TodoItem from '../components/TodoItem.jsx';
import "../style/containers/TodoList.scss";

// 模拟数据
// let state =[
// 	{
// 		id: 1,
// 		isDone: true,
// 		time: "11:00",
// 		thing: "准备叫外卖"
// 	},
// 	{
// 		id: 2,
// 		isDone: true,
// 		time: "12:00",
// 		thing: "准备吃午饭"
// 	},
// 	{
// 		id: 3,
// 		isDone: false,
// 		time: "1:00",
// 		thing: "准备坐地铁"
// 	}
// ]
// let item = {
//     isDone: true,  // 导出组件类型
//     time: "12:00",
//     thing: "准备吃午饭",
// }

class TodoList extends React.Component{
	constructor(props) {
		super(props);
		
	}
	render(props) {
		return (
			<div className="TodoList">
				<h1>任务清单</h1>
				<TodoItem />
				<button className="getItem">获取任务清单</button>
			</div>
			);
	}
}

export default TodoList;