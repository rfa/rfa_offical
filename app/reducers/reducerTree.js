/**
 *  reducer 集合器
 **/

import { combineReducers } from 'redux';
import { routerReducers } from 'react-router-redux';

/** 引入各个板块的 reducer **/

// index
// ... 

// TodoList
import TodoList from './TodoList/TodoList.reducer';

/*** 合成 reducer 树 ***/
const Reducers = combineReducers({
	TodoList: TodoList,
	// index
	/**
	 * index: combineReducers({
	 * 	   index1,
	 *	   index2,
	 *     ...	
	 * })
	 **/

	routing: routersReducer
})

export default Reducers;
